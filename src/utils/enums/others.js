export const AVAILABLE_LANG = ["fr", "en"];

export const routesToExclude = ['/fr/logout','/fr/register', '/fr/login', '/fr/reset-password', '/', '/fr',
    '/fr/change-password', '/fr/contact'];
