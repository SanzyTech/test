const SERVER_URL = process.env.REACT_APP_SERVER_URL;


export const REACT_APP_API_COMPANY =  SERVER_URL +  "/api/v1/company";
export const REACT_APP_API_CUSTOMER = SERVER_URL + "api/v1/users" ;
export const REACT_APP_API_CUSTOMER_PROFILE = SERVER_URL +  "api/v1/users/profile/me";
export const REACT_APP_API_OFFERS = SERVER_URL + "api/v1/offers";
export const REACT_APP_CUSTOMER_SUBSCRIPTION = SERVER_URL + "api/v1/subscriptions";
export const REACT_APP_CUSTOMER_LOGIN = SERVER_URL+ "api/v1/authentification/login/client";
export const REACT_APP_CUSTOMER_ORDER = SERVER_URL + "api/v1/orders";
export const REACT_APP_CUSTOMER_DOCUMENT = SERVER_URL + "api/v1/documents";
export const REACT_APP_EVALUATION = SERVER_URL + "api/v1/evaluations";
export const REACT_APP_API_MEETING = SERVER_URL + "api/v1/meetings";
