export const setLocation = userInput => {
    let location = {};
    if (userInput.trim() !== "") {
        userInput = userInput.toLowerCase().replace(/[êéè]/g, 'e');
        userInput = userInput.split(',');
        if (userInput.length === 2) {
            location = {"town": userInput[0].trim(), "street": ""};
        } else if (userInput.length === 3) {
            location = {"street": userInput[0].trim(), "town": userInput[1].trim()};
        } else if (userInput.length === 4) {
            location = {"street": userInput[0].trim(), "town": userInput[2].trim()};
        }
    } else {
        location = {"town": null, "street": null};
    }
    return location;
};

export const isFloat = (n) => {
    return Number(n) === n && n % 1 !== 0;
};

export const toTheGreater = (num) => {
    return isFloat(num) ? Math.floor(num) + 1 : num
};

export const conditionsPassword = pass => {
    let valid = true;
    let score = 0;
    if (!pass)
        return {valid: false, score: 0};
    //The password should have at least 8 characters
    if (pass.length < 8) {
        return {valid: false, score: score};
    } else {
        // award every unique letter until 5 repetitions
        score = 30;
        let letters = {};
        for (let i = 0; i < pass.length; i++) {
            letters[pass[i]] = (letters[pass[i]] || 0) + 1;
            score += 5.0 / letters[pass[i]];
        }
        // points for mixing it up
        let variations = {
            digits: /\d/.test(pass),
            lower: /[a-z]/.test(pass),
            upper: /[A-Z]/.test(pass)
        };
        let variationCount = 0;
        for (let check in variations) {
            if (variations[check] === true) {
                variationCount += 1;
            } else {
                valid = false;
            }
        }
        score += (variationCount - 1) * 10;
        return {
            valid: valid,
            score: score
        };
    }
};


