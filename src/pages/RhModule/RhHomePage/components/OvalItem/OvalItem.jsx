import * as styles from "./OvalItem.module.css";
const OvalItem = (props) => {
    return (
        <>
            <div className={styles.OvalItem}>
                <h3>{props.name}</h3>
                <span className={styles.TicketDone}>{props.done}</span>
                <span className={styles.TicketPending}>{props.pending}</span>
            </div>
        </>
    )
}

export default OvalItem;