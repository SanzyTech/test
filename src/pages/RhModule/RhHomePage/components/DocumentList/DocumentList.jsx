import * as styles from "./DocumentList.module.css";
import { getAllDocument } from "../../../../../services/document";
import { useQuery } from "react-query";
import DocumentItem from "../DocumentItem/DocumentItem"
import loading from "../../../../../assets/icons/loading.gif"

const DocumentList = () => {

    const {data:listData, isLoading:listLoading} = useQuery('listDocuments',getAllDocument);

    return (
        <>
            <h1>Liste des documents à évaluer </h1>

            <div>
                    {listLoading?
                        <div style={{textAlign:'center', marginTop:'105px'}}>
                            <img src={loading} alt="loading " />
                        </div>
                        :
                         <ul >
                            {listData.Documents.map((document, index) =>(
                                    <DocumentItem
                                    key={index}
                                    id={document._id}
                                    description={document.description}
                                    type={document.type}
                                    creation_date={document.creation_date}
                                    id_subscription={document.id_subscription}
                                    Consultation={() => {
                                        console.log(document._id)
                                        localStorage.setItem("currentDocument",JSON.stringify(document))
                                    }}
                                />
                            ))}
                        </ul> 
                    }    
                </div>    
        </>
    )
}

export default DocumentList