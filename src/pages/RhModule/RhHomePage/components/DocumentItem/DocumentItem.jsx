import * as styles from "./DocumentItem.module.css";
import { Link } from "react-router-dom";
import {getIdOfferForSubscription} from "../../../../../services/subscription";
import { useState, useEffect } from 'react';


const DocumentItem = (props) => {
    const [offerDocumentName, setOfferDocumentName] = useState("")

    const fetchOfferDocumentName=async()=> {
        setOfferDocumentName(await getIdOfferForSubscription(props.id_subscription));
    } 

    useEffect(() => {
        fetchOfferDocumentName()
      });
    return (
        <>
             <div className={styles.DocItem}>

                <span className={styles.DocItemLeft}>
                    <span>
                        <span className={styles.ItemText}><strong>Nom: </strong> {props.description}</span>
                    </span>
                    <span className={styles.ItemText}><strong>Type:</strong>  {props.type}</span>
                    <span  className={styles.ItemText}><strong>Offre:</strong>  {offerDocumentName}</span>
                    <span  className={styles.ItemText}><strong>Crée le:</strong>  {props.creation_date}</span>
                    <span>
                        <Link to="/rh-evaluation"><button className={styles.ButtonConsulter} onClick={props.Consultation}>Consulter</button></Link>
                    </span>
                </span> 
                <span className={styles.DocItemRight}></span>
            </div>
        </>
    )
}

export default DocumentItem;