import * as styles from "./HeaderHome.module.css";
import OvalItem from "../OvalItem/OvalItem";

const HeaderHome = () => {
    return (
        <>
            <div style={{display:'flex',flexDirection:'row', justifyContent:'space-between'}}>
            <div className={styles.HeaderHome}>
                <h2 className={styles.HeaderHomeTitle}>Evaluation</h2>
                <div  style={{display:'flex',flexDirection:'row', justifyContent:'space-around'}}>
                    <OvalItem
                        name="NYAMA"
                        done="8"
                        pending="25"
                    />
                    <OvalItem
                        name="BRISS"
                        done="20"
                        pending="14"
                    />
                   <OvalItem
                        name="GLOIRE"
                        done="80"
                        pending="250"
                    />
                </div>
            </div>
            <div className={styles.HeaderHome}>
                <h2 className={styles.HeaderHomeTitle}>Rendez-vous</h2>
                <div style={{display:'flex',flexDirection:'row', justifyContent:'space-around'}}>
                <OvalItem
                        name="En cours"
                        done="8"
                        pending="25"
                    />
                    <OvalItem
                        name="Annulé"
                        done="20"
                        pending="14"
                    />
                   <OvalItem
                        name="Effectué"
                        done="80"
                        pending="250"
                    />
                </div>
            </div>
            </div>
        </>
    )
}

export default HeaderHome;