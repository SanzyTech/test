import * as styles from "./RhHomePage.module.css";
import HeaderHome from "../components/HeaderHome/HeaderHome";
import DocumentList from "../components/DocumentList/DocumentList";

const RhHomePage = () => {

    document.title = "Jobaas CV | RH ";
    return (
        <>
            <HeaderHome/>
            <div>
            <DocumentList/>
            </div>
        </>
    )
}

export default RhHomePage;