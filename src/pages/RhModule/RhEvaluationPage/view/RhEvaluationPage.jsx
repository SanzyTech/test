import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Button } from "antd";
import EvaluationItem from "../components/EvaluationItem/EvaluationItem";
import EvaluationForm from "../components/EvaluationForm/EvaluationForm";
import loading from "../../../../assets/icons/loading.gif"
import { ArrowLeftOutlined } from '@ant-design/icons';



const RhEvaluationPage = () => {

    const [currentDocument, setCurrentDocument] = useState({});
    const [formEvaluationVisible, setIsVisible] = useState(false);
    const [evaluationStatus, setEvaluationStatus ] = useState(false);
    // const [subscriptionConcerned, setSubscriptionConcerned] = useState({});
    const [isLoading, setIsLoading] = useState(false)

    const evaluationSucceed = (data) => {
        localStorage.removeItem("currentDocument")
        setEvaluationStatus(data)
    }

    useEffect(()=> {
        setCurrentDocument(JSON.parse((localStorage.getItem("currentDocument"))));
          setIsLoading(true)
    },[]);

    return (
        <>  
        <Link to="/rh-home"><Button style={{marginTop:"15px"}} shape="round" type="primary" icon={<ArrowLeftOutlined />}> Retour</Button></Link>
            <div style={{display:'flex', flexDirection:"row", justifyContent:'space-around'}}>
                <section>
               
                   <span>
                     {
                        !isLoading ?
                        (
                            <div style={{textAlign:'center', marginTop:'120px'}}>
                                <img src={loading} alt="loading " />
                            </div>
                        ):
                        (   
                        
                            !evaluationStatus&& <div> 
                                <h1>Informations sur le document </h1>
                                <EvaluationItem
                                creation_date = {currentDocument.creation_date}
                                type={currentDocument.type}
                                description={currentDocument.description}
                                id_subscription= {currentDocument.id_subscription}
                            ShowEvaluation={()=> {
                                setIsVisible(true)
                            }}/>
                            </div>
                           
                        )
                     }
                   </span>
                </section>
               {
                formEvaluationVisible&& <section>

                <EvaluationForm
                    id_user={currentDocument.id_user}
                    id_document={currentDocument._id}
                    id_subscription={currentDocument.id_subscription}
                    isSucceed={evaluationSucceed}
                />
            </section>
               }
            </div>
        </>
    )
}

export default RhEvaluationPage;