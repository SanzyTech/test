import * as styles from "./EvaluationItem.module.css";
import { Card } from "antd";


const EvaluationItem = (props) => {

    return (
        <>
           
                    <Card
                    hoverable
                    style={{ width: 750}}>
                        <div style={{display:"flex", flexDirection:"column"}}>
                            <div className={styles.CoverCard}></div>
                            <div >
                                <h3 style={{textAlign:'center'}}>Informations</h3>
                                <div style={{display:'flex', flexDirection:'column', fontWeight:'bold', fontSize:'1.2em', justifyContent:'space-between'}}>
                                    <span>Nom: {props.description} </span>
                                    <span>Date de la demande: {props.creation_date} </span>
                                    <span>Type: {props.type} </span>
                                </div>
                               <span>
                                    <button className={styles.EvaluationButton} onClick={props.ShowEvaluation}>Evaluer le document</button>
                               </span>
                            </div>
                        </div>
                    </Card>
        </>
    )
}

export default EvaluationItem;