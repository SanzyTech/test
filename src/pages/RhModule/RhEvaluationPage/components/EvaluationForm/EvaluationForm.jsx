import * as styles from "./EvaluationForm.module.css"
import { Card, Form, Tag} from "antd";
import {useForm} from 'react-hook-form'
import { useState } from "react";
import { useMutation } from "react-query";
import { createEvaluation } from "../../../../../services/evaluations";
import {SyncOutlined} from "@ant-design/icons";
import AlertMessage from "../../../../../commons/components/AlertMessage";


const EvaluationForm = (props) => {

    document.title = 'Jobaas CV | Evaluation';
    
    const {register, handleSubmit, formState,errors} = useForm({mode: "onChange"});
    const [message, setMessage] = useState('');
    const [succeed, setSucceed] = useState(false);
    const {isValid} = formState;

    const [mutate, {isLoading, isError}] = useMutation(
        createEvaluation,
        {
            onSuccess: () => {
                window.scrollTo(0, 0);
                console.log("SUUUUUUUCESSSSSSS");
                props.isSucceed(true);
                setSucceed(true);
                setMessage("Un email a été envoyé au client avec l'informations sur l'évaluation de son document !! ");
            },
            onError: (error) => {
                setMessage(error.message);
                console.log(error.message);
                window.scrollTo(0, 0);
            }
        }
    );

    const onSubmit = async(dataForm) => {
        console.log(localStorage.getItem("currentDocument"));
        const dataEvaluation = {

            comment: [dataForm.remark_fond,dataForm.remark_form],
            id_rh: localStorage.getItem("idUser"),
            id_subscription: props.id_subscription,
            id_document: props.id_document,
            mark: dataForm.mark,
            id_user:props.id_user,

        };
        await mutate(dataEvaluation);
        console.log(dataEvaluation)
    }

    return (
        <>
            {
                !succeed ?
                (                
                    <Card
                    hoverable
                    style={{ width: 750}}>
                        <div style={{display:"flex", flexDirection:"column"}}>
                        <Form layout="vertical" onSubmitCapture={handleSubmit(onSubmit)} >
        
                            <Form.Item label="Remarque sur le plan de forme"  rules={[{ required: true}]} required >
                            <span style={{display:'flex', flexDirection:'column'}}>
                            <span> 
                                <textarea
                                 className={styles.TextAreaForm}
                                  placeholder="Entrez vos remarques sur le plan de la forme" 
                                  ref={register({required:"Une Description de votre remarque sur le plan de la forme est obligatoire ! "})} 
                                  name="remark_form" 
                                />
                                </span>
                                {errors.remark_form && <small className={styles.Error}>{errors.remark_form.message}</small>}
                            </span>
                            </Form.Item>
        
                            <Form.Item label="Remarque sur le plan du fond"  rules={[{ required: true}]} required >
                            <span style={{display:'flex', flexDirection:'column'}}>
                            <span> 
                                <textarea
                                    className={styles.TextAreaForm}
                                    placeholder="Entrez vos remarques sur le plan du fond" 
                                    ref={register({required:"Une Description de votre remarque sur le plan du fond est obligatoire ! "})} 
                                    name="remark_fond" 
                                />
                                </span>
                                {errors.remark_fond && <small className={styles.Error}>{errors.remark_fond.message}</small>}
                            </span>
                            </Form.Item>
        
        
                            <Form.Item label="Note (/10) à attribuer à ce document"  rules={[{ required: true}]} required >
                                <span style={{display:'flex', flexDirection:'column'}}>
                                    <span>
                                        <input
                                            type="number" 
                                            name="mark" 
                                            placeholder="Note (/10)"
                                            ref={register({required:"Une Note est obligatoire ! "})} 
                                        />
                                    </span>
                                    {errors.mark && <small className={styles.Error}>{errors.mark.message}</small>}
                                </span>
                            </Form.Item>
        
                    <Form.Item>
                        {!isLoading ?
                                                (
                                                    <>
                                                        <button className={styles.ButtonConfirmEvalaution} disabled={!isValid}>Confirmer l'évaluation</button>
                                                    </>
                                                ) :
                                                (
                                                    <>
                                                        <Tag icon={<SyncOutlined spin/>}
                                                             style={{color: "white", fontSize: "large"}}
                                                             className={styles.buttonLogin}>
                                                            Validation en cours
                                                        </Tag>
                                                    </>
                                                )
                                            }
                    </Form.Item>
                    </Form>    
        
                        
                    </div>
                    </Card>
                )
                :
                (
                <div style={{padding:'25px', marginRight:'400px'}}>
                    <AlertMessage
                        message="Votre évaluation a été effectué avec succès"
                        description={message}
                        isSucceed={true}
                    />
                </div>
                )
            }
        </>
    )
}

export default EvaluationForm;