import MeetingList from "../components/MeetingList/MeetingList";
import CreatorMeeting from "../components/CreatorMeeting/CreatorMeeting";
import * as styles from "./MeetingPage.module.css";

const MeetingPage = () => {
    return (
        <>
            <div className={styles.MeetingPage}>
                <MeetingList/>
                <CreatorMeeting/>
            </div>
        </>
    )
}

export default MeetingPage;