import {Card, Badge} from "antd";
import * as styles from "./MeetingItem.module.css"

const MeetingItem = (props) => {
    return (
        <>
            {
                props.status == 0 ?
                (
                    <div className={styles.CardSection}>
                        <Card 
                            title={props.object} 
                            hoverable
                            style={{ width: 450, height:200 }}
                            headStyle={{backgroundColor:'#293241', height:10, color:'white', fontWeight:'bold', textAlign:'center'}}>
                            <span>
                                <p>Date: {props.date}</p>    
                                <p>Heure: {props.hour_meeting}</p>
                            </span>
                            <span>
                                <button className={styles.DetailButton}>Détails</button>
                                <button className={styles.ModidyButton} >Modifier</button>
                            </span>
                            <span className={styles.Badge}><Badge.Ribbon text="A valider" color="primary"></Badge.Ribbon></span>
                        </Card>
                    </div>
                )
                :(
                    <div className={styles.CardSection}>
                        <Card 
                            title={props.object} 
                            hoverable
                            style={{ width: 450, height:200 }}
                            headStyle={{backgroundColor:'#293241', height:10, color:'white', fontWeight:'bold', textAlign:'center'}}>
                            <span>
                            <p>Date: {props.date}</p>    
                                <p>Heure: {props.hour_meeting}</p>
                            </span>
                            <span>
                                <button className={styles.DetailButton}>Détails</button>
                                <button className={styles.ModidyButton} >Modifier</button>
                            </span>
                            <span className={styles.Badge}><Badge.Ribbon text="Annulé" color="red"></Badge.Ribbon></span>
                        </Card>
                    </div>
                )
            }

            
        </>
    )
}

export default MeetingItem;