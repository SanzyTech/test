import * as styles from "./MeetingList.module.css";
import { useQuery } from "react-query";
import MeetingItem from "../MeetingItem/MeetingItem";
import loading from "../../../../../assets/icons/loading.gif";
import { getUserMeeting } from "../../../../../services/meeting";

const MeetingList = () => {
    const {data:listData, isLoading:listLoading} = useQuery('listUserMeeting',getUserMeeting);

    return (
        <>
            <div>
                <h2 className={styles.Title}>Liste des rendez-vous</h2>
                <div>
                    {listLoading?
                        <div style={{textAlign:'center', marginTop:'105px'}}>
                            <img src={loading} alt="loading " />
                        </div>
                        :
                         <ul>
                            {listData.map((meeting, index) =>(
                                    <MeetingItem
                                    key={index}
                                    object={meeting.object}
                                    status={meeting.status}
                                    creation_date={meeting.creation_date}
                                    date={meeting.date}
                                    hour_meeting ={meeting.hour_meeting}
                                />
                            ))}
                        </ul> 
                    }    
                </div>  
                
            </div>
        </>
    )
}

export default MeetingList;