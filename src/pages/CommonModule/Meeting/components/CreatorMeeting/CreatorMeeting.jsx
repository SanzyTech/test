import * as styles from "./CreatorMeeting.module.css";
import {Form} from 'antd';
import {useForm} from 'react-hook-form';
import {useMutation} from "react-query"
import { useState } from "react";
import { createMeeting } from "../../../../../services/meeting";
import AlertMessage from "../../../../../commons/components/AlertMessage";

const CreatorMeeting = () => {

    const {register, handleSubmit, formState,errors} = useForm({mode: "onChange"});
    const {isValid} = formState;
    const [message, setMessage] = useState('');
    const [succeed, setSucceed] = useState(false);

    const [mutate, {isLoading, isError}] = useMutation(
        createMeeting,
        {
            onSuccess: () => {
                window.scrollTo(0, 0);
                console.log("SUUUUUUUCESSSSSSS");
                setSucceed(true);
                setMessage("Un email vous a été envoyé avec les informations sur votre demande de meeting !! ");
            },
            onError: (error) => {
                setMessage(error.message);
                console.log(error.message);
                window.scrollTo(0, 0);
            }
        }
    );

    const onSubmit = async(dataForm) => {
        const dataMeeting = {
            object : dataForm.object,
            date: dataForm.date_meeting,
            hour_meeting: dataForm.hour_meeting,
            participants: {
                id_user: localStorage.getItem("idUser")
            }
        };
        await mutate(dataMeeting);
        console.log(dataMeeting)
    }


    return (
        <>
            <div className={styles.CreatorItem}>
                    {
                        !succeed?
                        (
                            <div className={styles.CreatorSection}>
                            <h3 className={styles.TitleSection}> Programmer un rendez-vous</h3>
        
                            <Form layout="vertical" onSubmitCapture={handleSubmit(onSubmit)}>
        
                                <Form.Item label="Objet"  rules={[{ required: true}]} required className={styles.FormLabel}> <br />
                                <span style={{display:'flex', flexDirection:'column'}}>
                                     <span><input className={styles.InputMeeting}  placeholder="entrer l'objet de la reunion " ref={register({required:"L'objet est obligatoire ! "})} name="object" /></span>
                                    {errors.object && <small className={styles.Error}>{errors.object.message}</small>}
                                </span>
                                </Form.Item>
        
                                <Form.Item label="Date"  rules={[{ required: true}]} required className={styles.FormLabel}> <br />
                                <span style={{display:'flex', flexDirection:'column'}}>
                                     <span><input type="date" className={styles.InputMeeting}  ref={register({required:"La date est obligatoire ! "})} name="date_meeting" /></span>
                                    {errors.date_meeting && <small className={styles.Error}>{errors.date_meeting.message}</small>}
                                </span>
                                </Form.Item>
        
                                <Form.Item label="Heure"  rules={[{ required: true}]} required className={styles.FormLabel}> <br />
                                <span style={{display:'flex', flexDirection:'column'}}>
                                     <span><input type="time" className={styles.InputMeeting}  ref={register({required:"L'heure de la reunion est obligatoire ! "})} name="hour_meeting" /></span>
                                    {errors.hour_meeting && <small className={styles.Error}>{errors.hour_meeting.message}</small>}
                                </span>
                                </Form.Item>
        
                                <Form.Item>
                                <button style={{marginTop: '30px'}} disabled={!isValid}>Confirmer la demande de reunion</button>
                                </Form.Item>
        
                            </Form>
                            </div>
                        )
                        :
                        (
                            <div style={{padding:'25px', marginRight:'400px'}}>
                                 <AlertMessage
                                    message="Votre évaluation a été effectué avec succès"
                                    description={message}
                                    isSucceed={true}
                                />
                            </div>
                        )
                    }
                </div>
        </>
    )
}

export default CreatorMeeting;