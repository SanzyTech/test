import React from 'react';

const ForbiddenPage = () => {

    return(
        <section>
            <Result
                status="403"
                title="403"
                subTitle="Vous n'êtes pas authorisé à accéder à cette page contacter le +237690637982"
                extra={<Button type="primary"><Link to="/"> Retour à l'accueil</Link></Button>}
            />,
        </section>
    );
}

export default ForbiddenPage;