import React from 'react';
import * as styles  from "./HomePage.module.css";
import Header from "../../../commons/components/Header";

const HomePage = () => {

    return (
        <div>
            <Header/>
        </div>
    );
};

export  default HomePage;