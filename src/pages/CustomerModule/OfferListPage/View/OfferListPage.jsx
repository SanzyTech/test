import * as styles from "./OfferListPage.module.css";
import React, {useEffect, useState} from "react";
import Offeritem from "../Components/OfferItem/OfferItem";
import { useSelector } from "react-redux";
import { currentUserSelector } from "../../../../redux/AuthReducer";
import ModalFormSubscription from "../Components/ModalFormSubscription/ModalFormSubscription";
import {useQuery} from 'react-query';
import loading from "../../../../assets/icons/loading.gif";
import {getAllOffers} from '../../../../services/offer';
import {createSubscription} from '../../../../services/subscription'
import * as moment from 'moment';
import AlertMessage from '../../../../commons/components/AlertMessage';

const OfferListPage = () => {

    document.title = "Jobaas CV | Effectuer votre choix";
    const [isVisible, setIsVisible] = useState(false);
    const [textNotification, setTextNotification] = useState("");
    const [isSubscribe, setIsSubscribe] = useState(false);
    const [load, setLoad] = useState(false);
    const currentUser = useSelector(currentUserSelector);
    const {data:listData, isLoading:listLoading} = useQuery('listOffers',getAllOffers);

    
    const subscribeOffer = async ()=> {
        setLoad(true);
        const list =  localStorage.getItem("listOfSubscription")
        var listOfSubscription = [] 
        listOfSubscription.push(list)

        const subscription = {
            id_user:currentUser.id,  
            id_offer:localStorage.getItem("id_offer"),
            total : localStorage.getItem("totalSubscription"), 
            list_of_subscription: listOfSubscription 
        }

       try {
            await createSubscription(subscription);
            setTextNotification(" Votre souscription à été prise en compte") ;

       }catch(e) {
            setTextNotification(e);
            setLoad(false);
       }
       setLoad(false);
       setIsVisible(false);
       setIsSubscribe(true);
       setTimeout(()=> {
        window.location.reload()
       },1500)
    }

    const cancelModal = () => {
        setIsVisible(false);
    }

    useEffect(() => {
        window.scrollTo(0, 0);
        moment.locale('fr');
    }, []);

    return (
        <>
        {!isSubscribe ?(
            <>
            <section className={styles.Body}>
                <h1 className={styles.TitlePage}>Choisir une offre</h1>
                <div>
                    {listLoading?
                        <div style={{textAlign:'center', marginTop:'105px'}}>
                            <img src={loading} alt="loading " />
                        </div>
                        :
                         <ul className={styles.OffreList}>
                            {listData.Offers.map((offer, index) =>(
                                    <Offeritem
                                        key={index}
                                        id={offer._id}
                                        name={offer.name}
                                        description={offer.description}
                                        price={offer.price}
                                        showModal={()=>
                                            {setIsVisible(true);
                                            localStorage.setItem("id_offer", offer._id);
                                            localStorage.setItem("totalSubscription", offer.price);
                                            localStorage.setItem("listOfSubscription", offer.name)
                                        }
                                        }
                                    />
                            ))}
                        </ul> 
                    }    
                </div>    
                    <ModalFormSubscription
                    isVisible={isVisible}
                    cancelModal={cancelModal}
                    subscribeOffer={subscribeOffer} 
                    isLoading={load}
                    />

            </section>
        </>
        ):
        (
            <>
            <div style={{padding:'25px'}}>
                <AlertMessage
                    message={textNotification}
                    isSucceed={true}
                />
            </div>
        </>

        )
        }
        </> 
    )
}

export default OfferListPage;