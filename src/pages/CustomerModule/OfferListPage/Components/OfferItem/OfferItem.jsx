import * as styles from "./OfferItem.module.css";

const Offeritem = (props) => {
    return (
        <>
            <div className={styles.OfferItem}>
                <span style={{textAlign:'center', fontSize:'1.8em', marginBottom:'10px', fontWeight:'bold'}}>{props.name}</span>
                <span>{props.description}</span>
                   <button className={styles.ButtonOffre} onClick= {props.showModal }>Souscris</button>
                { props.price !== 0?(
                    <span className={styles.Prix}>{props.price} FCFA</span>
                ):
                (
                    <span className={styles.Prix}>Gratuite</span>
                )
                }   
            </div>
        </>
    )
}
export default Offeritem;
