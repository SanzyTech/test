import {Modal, Button} from 'antd';
import { useState } from 'react';
import * as styles from './ModalFormSubscription.module.css';
import { Upload, message } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
// import type { UploadProps } from 'antd';
import { useSelector } from 'react-redux';
import { currentUserSelector } from '../../../../../redux/AuthReducer';


const ModalFormSubscription = (props) => {

    const [loading, setLoading]= useState(false);
    const currentUser = useSelector(currentUserSelector);
    return(
        <>
            <Modal title="Souscription à l'offre"
                    //  onOk={subscribeOffer} 
                    visible={props.isVisible}
                    onCancel={props.cancelModal}
                     footer={[
                        <Button key="back" onClick={props.cancelModal}>
                        Annuler
                      </Button>,
                      <Button key="submit"  type="primary" onClick={props.subscribeOffer} loading={props.isLoading} >
                        Valider
                      </Button>
                     ]}>
                        <p>Pour profiter pleinement de cette offre, veuillez renseigner votre document (CV,lettre de motivation)</p>
                        <div>
                        <span>
                            <span className={styles.SingleInput}>
                                <label htmlFor="nom">Nom </label>
                                <input disabled className={styles.InputModal} value={currentUser.name} name="nom"/>
                            </span>
                            <span className={styles.SingleInput}>
                                <label htmlFor="prénom">Prénom </label>
                                <input disabled className={styles.InputModal} value={currentUser.surname} name="prénom"/>
                            </span>
                        </span>
                        <span>
                            <span className={styles.SingleInput}>
                                <label htmlFor="téléphone">Téléphone </label>
                                <input disabled className={styles.InputModal} value={currentUser.phone.value} name="téléphone"/>
                            </span>
                            <span className={styles.SingleInput}>
                                <label htmlFor="téléphone">Email </label>
                                <input disabled className={styles.InputModal} value={currentUser.email} name="email"/>
                            </span>
                        </span>
                        </div>
                     </Modal>
        </>
    )
}

export default ModalFormSubscription