export const offerlist = [
    {
        name:'NYAMA',
        description:' Cette offre est gratuite. Le recruteur fera une évaluation brève du CV de l’étudiant en lui présentant les erreurs commises, sans toutefois les corriger. Pour ce faire, l’étudiant/chercheur d’emploi cliquera sur le bouton « Evaluation du Cv ». Puis, un formulaire lui permet d’uploader son CV. (Celui-ci sera stocké dans la plateforme). A l’envoi du formulaire, le recruteur recevra le CV par mail. Il pourra ensuite le consulter et attribuer une note.',
        price:'GRATUIT'
    },

    {
        name:'GLOIRE DES GLOIRES',
        description:' Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam,',
        price:'400 FCFA'
    },

    {
        name:'BRISS',
        description:' Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam,',
        price:'350 FCFA '
    },
]
