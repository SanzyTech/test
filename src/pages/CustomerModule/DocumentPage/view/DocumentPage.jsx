import { useEffect } from "react";
import moment from "moment";
import { getUserDocument } from "../../../../services/document";
import loading from "../../../../assets/icons/loading.gif";
import DocumentItem from "../components/DocumentItem";
import { useQuery } from "react-query";
import * as styles from "./DocumentPage.module.css"


const DocumentPage = () => {
    document.title = "Jobaas CV | Vos documents";

    const {data:listData, isLoading:listLoading} = useQuery('listDocuments',getUserDocument);
    const viewDetail = () => {
        
    }

    useEffect(() => {
        window.scrollTo(0, 0);
        moment.locale('fr');
    }, []);


    return (
        <div className={styles.DocumentPage}>
            <section className={styles.LeftSection}>
            <h1>Liste de vos documents</h1>
            <div>
                    {listLoading?
                        <div style={{textAlign:'center', marginTop:'105px'}}>
                            <img src={loading} alt="loading " />
                        </div>
                        :
                         <ul >
                            {listData.Documents.map((document, index) =>(
                                    <DocumentItem
                                        key={index}
                                        id={document._id}
                                        creation_date={document.creation_date}
                                        description={document.description}
                                        type={document.type}
                                        viewDetail={viewDetail()}
                                    />
                            ))}
                        </ul> 
                    }    
                </div> 
            </section>
                    <div>

                    </div>
            <section className={styles.RightSection}>
                   
            {listLoading?
                    (    <div style={{textAlign:'center', marginTop:'105px'}}>
                            <img src={loading} alt="loading " />
                        </div>
                    )
                    : 
                    (
                        <div>
                            <h1> Evaluation relative à votre document</h1>
                            
                        </div>
                    )
}            
            </section>
        </div>
    )
}

export default DocumentPage;