import * as styles from "./DocumentItem.module.css";

const DocumentItem = (props) => {
    return (
        <>
            <div className={styles.DocItem}>
                <span className={styles.DocItemLeft}>
                    <span className={styles.ItemText}><strong>Nom: </strong> {props.description}</span>
                    <span className={styles.ItemText}><strong>Type:</strong>  {props.type}</span>
                    <span  className={styles.ItemText}><strong>Crée le:</strong>  {props.creation_date}</span>
                    <span><button className={styles.ButtonDetail} onClick={props.viewDetail}>Détails</button></span>
                </span>
                <span className={styles.DocItemRight}></span>
            </div>
        </>
    )   
}

export default DocumentItem;