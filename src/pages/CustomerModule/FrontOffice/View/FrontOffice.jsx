import * as styles from "./FrontOffice.module.css";
import loading from "../../../../assets/icons/loading.gif";
import OfferListPage from "../../OfferListPage/View/OfferListPage";
import HomePage from "../../HomePage/view/HomePage";
import { useState, useEffect } from "react";
import { checkUserSubscriptionStatus } from "../../../../services/subscription";



const FrontOffice =() => {
    const [isLoading, setIsLoading] = useState(false);
    const [subscriptionStatus, setStatus] = useState(false);
    const fecthUserStatus=async(userId)=> {
        setStatus(await checkUserSubscriptionStatus(userId));
        setIsLoading(true)
    } 


    useEffect(() => {
        const userId = localStorage.getItem("idUser");
        fecthUserStatus(userId);

    },[])
     
    return (
        <div>
            {!subscriptionStatus ?
            (
                isLoading?
                
                (  
                    <OfferListPage/>
                )
                :
                
                (   <div style={{textAlign:'center', marginTop:'120px'}}>
                        <img src={loading} alt="loading " />
                    </div>
                )
            ):
            ( isLoading?
                
                (  
                    <HomePage/>
                )
                :
                
                (   
                    <div style={{textAlign:'center', marginTop:'120px'}}>
                        <img src={loading} alt="loading " />
                    </div>
                )
                
              
            )}
        </div>
    )
 }

export default FrontOffice