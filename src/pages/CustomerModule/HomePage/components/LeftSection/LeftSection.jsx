import * as styles from  './LeftSection.module.css';
import { Button } from 'antd';
import { UploadOutlined } from '@ant-design/icons';

const LeftSection = (props) => {
    return (
        <div className={styles.LeftSection}> 
        <div>
        <div className={styles.HeaderLeftSection} > <h1 className={styles.HomeTitle}> Bienvenue sur Jobaas CV</h1></div>
          <p style={{textAlign:'center', fontWeight:'bold', fontSize:'1.4em',marginTop:'30px'}}>Profitez  de notre accompagnement sur mesure</p>
          <p style={{textAlign:'center', fontSize:'1.8em',marginBottom:'40px'}}> Une RH fera une évaluation brève de ton CV/Lettre de motivation en te présentant les erreurs commises, sans toutefois les corriger</p>
          <p style={{textAlign:'center', fontWeight:'bold', fontSize:'1.2em',marginBottom:'40px'}}> Pour profiter d'un accompagnement spéciale et spécifique et profitez d'une multitude d'autres services, souscrivez à une de nos offres gratuites</p>
          {/* <Upload> */}
              <Button  onClick={props.showModal} className={styles.ButtonUpload} icon={<UploadOutlined />} >Click to Upload</Button>
          {/* </Upload> */}
        </div>
  </div>
    )
}

export default LeftSection