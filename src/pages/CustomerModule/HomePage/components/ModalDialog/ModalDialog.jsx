import { Modal, Button, Alert } from "antd";


const ModalDialog = (props) => {
    return (
        <>
              <Modal title="Upload de document"
                    visible={props.isVisible}
                    onCancel={props.cancelModal}
                     footer={[
                        <Button key="back" onClick={props.cancelModal}>
                        OK
                      </Button>,
                      <Button disabled={props.uploadStatus} key="submit"  type="primary" onClick={props.subscribeOffer} loading={props.isLoading} >
                        upload
                      </Button>
                     ]}>
                     {!props.uploadStatus?
                       (      
                        <div>
                          <p>Pour profiter pleinement de notre service d'accompagnement</p>
                          <input type="text"  value={props.documentName} onChange={props.handleChange}/>
                        </div>
                        ):
                        (
                          <div>
                            <Alert
                              message="Votre document a été upload avec succès. Une RH effectuera une évaluation ! "
                              description="Consultez vos documents pour y accéder !"
                              type="success" 
                              showIcon />
                          </div>
                        )
                      }
                     </Modal>
      </>
    )

}   

export default ModalDialog;