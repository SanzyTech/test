import * as styles from './RightSection.module.css';
import { Col, Row } from 'antd';
import { Card }  from 'antd';
import { Progress } from 'antd';
import { Alert } from 'antd';
import { Button } from 'antd';


const RightSection = (props) => {
    return ( 
        <div className={styles.RightSection}>
        <h2 className={styles.RightSectionTitle}>Evaluation</h2>
        <Card >
            <Progress percent={33} />
           <small className={styles.SignalForfait}> Vous avez droit à une seule évaluation de votre document</small>
        </Card>  
        <h2 className={styles.RightSectionTitle}>Vos rendez-vous</h2>
        <div className={styles.HistoriqueHome}>
            <Alert
                style={{textAlign:'center', 
                marginLeft:'auto', marginRight:'auto', 
                marginBottom:'20px',
                display:'flex',
                flexDirection:'column',
                }} 
                message="Vous n'avez aucun rendez-vous avec une Rh. Programmez un rendez-vous pour profiter de ce service "
                type="info"
                action= {
                    <Button size="small" type="primary" >
                    Programmer rendez-vous
                    </Button>
                }/>
        </div>
    </div>
    )
}

export default RightSection