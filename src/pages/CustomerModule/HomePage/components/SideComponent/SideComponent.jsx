import * as styles from "./SideComponent.module.css";
import { Avatar, Badge } from "antd";
import { UserOutlined } from "@ant-design/icons";
import { getOfferName } from "../../../../../services/offer";
import { useEffect, useState } from "react";



const SideComponent = () => {
    const [currentSubscription, setCurrentSubscription] = useState({})
    const [offerName, setOfferName] = useState("")

    const fetchOfferName = async()=> {
        setCurrentSubscription(JSON.parse((localStorage.getItem("currentSubscription"))))
        setOfferName(await getOfferName(currentSubscription.id_offer))
    } 

    
    // useEffect(() => {
    //     fetchOfferName()
    //   });

    return (
        <>

            <div className={styles.SideComponent}>
            <span className={styles.SideOfferBadge} > {offerName}</span>
            <span>
                <h4>Documents  <Badge count={55} style={{ backgroundColor: '#52c41a', borderColor:'#52c41a', marginLeft:'20px', marginTop:'10px' }}></Badge></h4>
                <h4>Evaluations  <Badge count={55} style={{ backgroundColor: '#52c41a', borderColor:'#52c41a', marginLeft:'20px', marginTop:'10px'   }}></Badge></h4>
                <h4>Rendez-vous  <Badge count={555} style={{ backgroundColor: '#52c41a', borderColor:'#52c41a', marginLeft:'20px' , marginTop:'10px'   }}></Badge></h4>
            </span>

            </div>
        </>
    )
}

export default SideComponent;