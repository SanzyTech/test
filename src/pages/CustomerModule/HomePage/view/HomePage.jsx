import ModalDialog from '../components/ModalDialog/ModalDialog';
import { useState } from 'react';
import LeftSection from '../components/LeftSection';
import RightSection from '../components/RightSection';
import * as styles from './HomePage.Module.css';
import { createDocument } from '../../../../services/document';
import { useEffect } from 'react';
import { getUserCurrentSubscription } from '../../../../services/subscription';
import SideComponent from '../components/SideComponent/SideComponent';

const HomePage = () => {
    
    const [isVisible, setIsVisible] = useState(false);
    const [load, setLoad] = useState(false);
    const [documentName, setDocumentName] = useState("");
    const [uploadStatus, setUploadStatus] = useState(false)

    const cancelModal = () => {
        setIsVisible(false);
    }

    const subscribeOffer = async() => {
        setLoad(true);
        const currentSubscription = JSON.parse(localStorage.getItem('currentSubscription'))
        const document = {
            description : String(documentName.value),
            id_user : localStorage.getItem('idUser'),  
            id_subscription : currentSubscription._id,
            url: "URL DOCUMENT",
            type: "CV"  
        } 
        console.log(document)
        try {
            await createDocument(document);
            console.log(document);
            setTimeout(() => {
                setLoad(false);
                setUploadStatus(true)
              }, 3000);

       }catch(e) {
            console.log(e);
       }
    }

    const handleChange =(event) => {
        setDocumentName({value: event.target.value});
    }

    const showModal = () => {
        setIsVisible(true)
        console.log(isVisible)
    };

    useEffect(() => {
        getUserCurrentSubscription();
      });

    return (
        <>
           <div className={styles.BodyHome}>
                    <SideComponent/>
                    <LeftSection
                    showModal= {showModal}/>
                    
                    <RightSection/>
 
                    <ModalDialog
                    isVisible={isVisible}
                    setDocumentName={documentName}
                    handleChange={handleChange}
                    cancelModal={cancelModal}
                    subscribeOffer={subscribeOffer} 
                    isLoading={load}
                    uploadStatus={uploadStatus}

                    />
           </div>
          
        </>
    )
}

export default HomePage