import * as styles from "./EvaluationPage.module.css";
import { useQuery } from "react-query";
import loading from "../../../../assets/icons/loading.gif"
import { getUserEvaluation } from "../../../../services/evaluations";
import EvaluationItem from "../components/EvaluationItem";
import { useState } from "react";

const EvaluationPage = () => {

    document.title = "Jobaas CV | Vos documents";

    const {data:listData, isLoading:listLoading} = useQuery('listEvaluations',getUserEvaluation);
    const [documentVisible, setDocumentVisible] = useState(false);


    return (
        <div className={styles.SectionEvaluation}>
            <div>
            <h1>Liste d'évaluations de vos documents</h1>
                    {listLoading?
                        <div style={{textAlign:'center', marginTop:'105px'}}>
                            <img src={loading} alt="loading " />
                        </div>
                        :
                         <ul >
                            {listData.Evaluations.map((evaluation, index) =>(
                                    <EvaluationItem
                                        key={index}
                                        id={evaluation._id}
                                        comment={evaluation.comment}
                                        mark={evaluation.mark}
                                        state={evaluation.state}
                                        creation_date={evaluation.creation_date}
                                        showDocument= {() => {console.log("show")}}
                                
                                    />
                            ))}
                        </ul> 
                    }    
            </div> 
        </div>
    )
}

export default EvaluationPage