import * as styles from "./EvaluationItem.module.css";
import { Badge } from "antd";

const EvaluationItem = (props) => {
    return (
         <div className={styles.EvaluationItem}>
             <div className={styles.HeaderEvaluationDone}> </div>
           <div className={styles.EvaluationItemSpace}>
           <div className={styles.EvaluationItemLeft}>
                {
                    props.comment?
                    (
                        <span className={styles.ItemText}><strong>Commentaire:</strong> {props.comment}</span>
                    ):
                    (
                        <span className={styles.ItemText}><strong>Commentaire:</strong> <em>en cours d'évaluation</em></span>
                    )
                }
               {
                    props.mark?
                    (
                        <span className={styles.ItemText}><strong>Note(/10):</strong> {props.mark}</span>
                    ):
                    (
                        <span><strong>Note(/10):</strong> <em>en cours d'évaluation</em></span>
                    )
                }
                <span className={styles.ItemText}><strong>Date de l'évaluation:</strong>{props.creation_date}</span>
                <span><button onClick={props.ShowDocument}></button></span>
            </div>
            <div className={styles.EvaluationItemRight}>
                <span> <Badge status="success" text="Evalué" /></span>
            </div>
           </div>
        </div>
    )
}
export default EvaluationItem;