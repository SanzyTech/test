import {CreateOrUpdateRequest} from "../utils/helpers/apiHelpers";
import {REACT_APP_API_CUSTOMER} from "../utils/enums/apiUrls"

export const createUser = async (user) => {

    const apiUrl = REACT_APP_API_CUSTOMER;
    const body = JSON.stringify(user);
    return await CreateOrUpdateRequest(apiUrl, body, 'post');
};
