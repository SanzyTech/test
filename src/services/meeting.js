import {CreateOrUpdateRequest, GetRequest} from "../utils/helpers/apiHelpers";
import {REACT_APP_API_MEETING} from "../utils/enums/apiUrls"

export const getAllMeeting = async () => {
    const apiUrl = REACT_APP_API_MEETING;
    const data = await GetRequest(apiUrl);
    console.log(data)
    return data;
}

export const getUserMeeting = async () => {
    
    const apiUrl = REACT_APP_API_MEETING;
    const data = await GetRequest(apiUrl);
    const meeting = [];
    const currentUser = localStorage.getItem("idUser")
    for (let index = 0; index < data.length; index++) {
        if (data.Meetings[index].participants.id_user == currentUser ){
            console.log(data.Meetings[index].participants.id_user)
          meeting.push(data.Meetings[index])
        }
    };
    console.log(meeting)
    return meeting;
}

export const createMeeting = async (meeting) => {
    const apiUrl = REACT_APP_API_MEETING;
    const body = JSON.stringify(meeting);
    return await CreateOrUpdateRequest(apiUrl, body, 'post');
}