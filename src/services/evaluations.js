import { GetRequest, CreateOrUpdateRequest} from "../utils/helpers/apiHelpers";
import {REACT_APP_CUSTOMER_DOCUMENT, REACT_APP_EVALUATION, } from "../utils/enums/apiUrls";

export const getUserEvaluation = async () => {
  const currentUser = localStorage.getItem("idUser")
  const apiUrl = localStorage.getItem('state') === "employee" ?
  `${REACT_APP_EVALUATION}/?id_user=${currentUser}` : `${REACT_APP_EVALUATION}/?id_rh=${currentUser}`
    const data = await GetRequest(apiUrl);
    return data;
}

export const createEvaluation = async (evaluation) => {
  const apiUrl = REACT_APP_EVALUATION;
  const body = JSON.stringify(evaluation);
  return await CreateOrUpdateRequest(apiUrl, body, 'post');
}


// export const getDocumentOfEvaluation = async(id) => {
//   console.log(id)
//   // const apiUrl = `${REACT_APP_CUSTOMER_DOCUMENT}/${id}`;
//   // const data = await GetRequest(apiUrl);
//   // console.log(data)
//   // return data;
// }