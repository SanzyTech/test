
import {CreateOrUpdateRequest} from "../utils/helpers/apiHelpers";
import {REACT_APP_CUSTOMER_LOGIN} from '../utils/enums/apiUrls'

export const login = async (data) => {
    const authData = JSON.stringify(data);
    localStorage.setItem('state', data.state);
    const apiUrl = REACT_APP_CUSTOMER_LOGIN;
    return await CreateOrUpdateRequest(apiUrl, authData, "post");
    
};
 