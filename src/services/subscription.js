import {CreateOrUpdateRequest, GetRequest} from "../utils/helpers/apiHelpers" ;
import {REACT_APP_CUSTOMER_SUBSCRIPTION} from "../utils/enums/apiUrls";
import { getOfferName } from "./offer";

const currentUser = localStorage.getItem("idUser");

export const createSubscription = async (subscription) => {
    const apiUrl = REACT_APP_CUSTOMER_SUBSCRIPTION;
    const body = JSON.stringify(subscription);
    await CreateOrUpdateRequest(apiUrl, body, 'post');

}

export const  getAllSubscriptions = async () => {
    const apiUrl = REACT_APP_CUSTOMER_SUBSCRIPTION;
    const data = await GetRequest(apiUrl);
    return data;
};

export const checkUserSubscriptionStatus = async (id) => {
    const apiUrl = `${REACT_APP_CUSTOMER_SUBSCRIPTION}/?id_user=${id}`;
    const data = await GetRequest(apiUrl);
    if (data.Subscriptions.length !== 0 ) {
        return true
    }else{
        return false}
}

export const getUserCurrentSubscription = async () => {
    const apiUrl = `${REACT_APP_CUSTOMER_SUBSCRIPTION}/?id_user=${currentUser}&status=en cours`;
    const data = await GetRequest(apiUrl);
    if (data.Subscriptions.length == 0 ) {
        localStorage.setItem("currentSubscription","AUCUNE SOUSCRIPTION") 
        return  "AUCUNE SOUSCRIPTION"
    }else {
        localStorage.setItem("currentSubscription", JSON.stringify(data.Subscriptions[0])) 
        console.log(data.Subscriptions[0]._id)
        return  data.Subscriptions[0]
    }
} 

export const getIdOfferForSubscription = async (id) => {
    const apiUrl = REACT_APP_CUSTOMER_SUBSCRIPTION + "/" + id;
    const data = await GetRequest(apiUrl);
    const offerName = await getOfferName(data.subscription.id_offer);
    return offerName;
}