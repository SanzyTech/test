import {CreateOrUpdateRequest, GetRequest} from "../utils/helpers/apiHelpers";
import {REACT_APP_CUSTOMER_DOCUMENT, REACT_APP_EVALUATION} from "../utils/enums/apiUrls";

const currentUser = localStorage.getItem("idUser");

export const createDocument = async (document) => {
    const apiUrl = REACT_APP_CUSTOMER_DOCUMENT;
    const body = JSON.stringify(document);
    await CreateOrUpdateRequest(apiUrl, body, 'post');

}

export const getUserDocument = async () => {
    const apiUrl = `${REACT_APP_CUSTOMER_DOCUMENT}/?id_user=${currentUser}`;
    const data = await GetRequest(apiUrl);
    return data;
    
}       

export const getAllDocument = async () => {
    const apiUrl = REACT_APP_CUSTOMER_DOCUMENT;
    const data = await GetRequest(apiUrl);
    console.log(data)
    return data;
}

export const getDocumentbyId = async (id) => {
    const apiUrl = REACT_APP_CUSTOMER_DOCUMENT + id;
    const data = await GetRequest(apiUrl)
    console.log(data);
}

export const setUserDocumentStatus = async() => {
    const currentDocument = localStorage.getItem("currentDocument")
    const apiUrl = REACT_APP_CUSTOMER_DOCUMENT/currentDocument;
    const body = {state:1}
    await CreateOrUpdateRequest(apiUrl, body, 'put');

}

export const getDocumentEvaluations = async(id) => {
    const apiUrl = `${REACT_APP_EVALUATION}/?id_document=${id}`;
    const data = await GetRequest(apiUrl);
    return data;

}