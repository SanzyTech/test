import { GetRequest} from "../utils/helpers/apiHelpers" ;
import {REACT_APP_API_OFFERS} from "../utils/enums/apiUrls";

export const  getAllOffers = async () => {
    const apiUrl = REACT_APP_API_OFFERS;
    const data = await GetRequest(apiUrl);
    console.log(data)
    return data;
};

export const getOfferName = async (id) => {
    console.log(id)
    const apiUrl = REACT_APP_API_OFFERS + "/" + id;
    const data = await GetRequest(apiUrl);
    return data.offer.name;
}
