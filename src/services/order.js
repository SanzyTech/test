import {CreateOrUpdateRequest} from "../utils/helpers/apiHelpers";
import {REACT_APP_CUSTOMER_ORDER} from "../utils/enums/apiUrls"

export const createOrder = async (data) => {
    console.log("++++++++++++")
    const apiUrl = REACT_APP_CUSTOMER_ORDER;
    const body = JSON.stringify(data);
    return await CreateOrUpdateRequest(apiUrl, body,'post' );
}