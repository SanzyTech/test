import React, { useRef } from 'react';
import * as styles  from "./RegistrationForm.module.css";
import { Form, Tag  } from 'antd';
import { Radio } from 'antd';
import {useState} from 'react';
import {createUser} from "../../../services/particular";
import lockIcon from '../../../assets/icons/lockIcon.png';
import emailIcon from '../../../assets/icons/emailIcon.png';
import profilIcon from '../../../assets/icons/profilIcon.png';
import genderIcon from '../../../assets/icons/genderIcon.png';
import {SyncOutlined} from "@ant-design/icons";
import {useForm} from 'react-hook-form';
import {useMutation} from "react-query";
import Link from 'react-router-dom'
import AlertMessage from "../AlertMessage";


const RegistrationForm = () => {

    document.title = 'Jobaas CV | Créer un compte';
    const [message, setMessage] = useState('');
    const [succeed, setSucceed] = useState(false);

    const password = useRef({});
    const [radioValue, setradioValue] = useState("Man");

    const onChangeRadio = (e) => {
        setradioValue(e.target.value);
    };


    const {register, handleSubmit, formState,errors,watch} = useForm({mode: "onChange"});
    password.current =watch("password","");
    const {isValid} = formState;


    const [mutate, {isLoading, isError}] = useMutation(
        createUser,
        {
            onSuccess: () => {
                window.scrollTo(0, 0);
                setSucceed(true);
                setMessage("Un email et un sms vous ont été envoyés afin de valider la création de votre compte. Pensez à regarder VOS SPAMS ou MESSAGES INDESIRES !! " +
                    "Le lien du sms vous fera valider  le numero et activera votre compte.\n Certains emails PEUVENT METTRE 15 MINUTES à arriver excusez nous pour le désagrément. ");
            },
            onError: (error) => {
                setMessage(error.message);
                console.log(error.message);
                window.scrollTo(0, 0);
            }
        }
    );


    const onSubmit = async(dataForm) => {
        const dataUser = {
            email: {
                value: dataForm.email
            },
            name: dataForm.last_name,
            profession:dataForm.profession,
            surname: dataForm.first_name,
            country:dataForm.country,
            gender: radioValue,
            town: dataForm.town,
            phoneNumber: {
                value: dataForm.phoneNumber
            },
            password: dataForm.password,
            birthday: dataForm.birthday,
            origin: dataForm.origin,
            state:["employee"],
            street: "test"
        };
        await mutate(dataUser);
        console.log(dataUser)
    }

    return (
        <div className={styles.FormSection}>  
           {!succeed ?
           (
               <p className={styles.TitleForm}> INSCRIPTION </p> 
            ):
            (
                <p className={styles.TitleFormSuccess}> INSCRIPTION REUSSIE </p>  
            )
            }
          
               
           {isError ?
                        (
                            <>
                                <AlertMessage message={message} isError={true}/>
                            </>
                        ) : null}

        {!succeed ?
            (
                <Form layout="vertical" onSubmitCapture={handleSubmit(onSubmit)} >

            <Form.Item label="Prénom" name="prénom" required className={styles.FormLabel} >
                <span style={{display:'flex', flexDirection:'column'}}>
                    <span><input className={styles.InputRegistration} placeholder="entrer votre prénom" name="first_name" ref={register({required:"Votre prénom est recquis "})} /> <img src={profilIcon} alt="" /> </span>
                    {errors.first_name && <small className={styles.Error}>{errors.first_name.message}</small>}
                </span>
            </Form.Item>
            
        
            <Form.Item label="Nom" name="nom" required className={styles.FormLabel}>
                <span style={{display:'flex', flexDirection:'column'}}>
                    <span><input className={styles.InputRegistration} placeholder="entrer votre nom" name="last_name" ref={register({required:"Votre nom est recquis "})} /> <img src={profilIcon} alt="" /></span>
                    {errors.last_name && <small className={styles.Error}>{errors.last_name.message}</small>}
                </span>
            </Form.Item>

            <Form.Item label="Pays d'origine" name="pays" required className={styles.FormLabel}>
                <span style={{display:'flex', flexDirection:'column'}}>
                    <span><input className={styles.InputRegistration} placeholder="Pays d'origine" name="country" ref={register({required:"Votre pays est recquis "})} /><img src={profilIcon} alt="" /></span>
                    {errors.country && <small className={styles.Error}>{errors.country.message}</small>}
                </span>
            </Form.Item>

            <Form.Item label="Ville"  name="ville" required className={styles.FormLabel}>
                <span style={{display:'flex', flexDirection:'column'}}>
                    <span><input className={styles.InputRegistration} placeholder="entrer votre ville" name="town" ref={register({required:"Votre nom est recquis "})} /><img src={profilIcon} alt="" /></span>
                    {errors.town && <small className={styles.Error}>{errors.town.message}</small>}
                </span>
            </Form.Item>

            <Form.Item label="Adresse mail"  name="mail" required className={styles.FormLabel} >
                <span style={{display:'flex', flexDirection:'column'}}>
                    <span><input className={styles.InputRegistration} placeholder="entrer votre adresse email" ref={register({required:"Votre adresse email est recquise",pattern: {
          value: /^(([^<>()\\[\]\\.,;:\s@"]+(\.[^<>()\\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
          message: 'Veuillez entrer une adresse email valide'}})} name="email" /> <img src={emailIcon} alt="" /></span>
                    {errors.email && <small className={styles.Error}>{errors.email.message}</small>}
                </span>
            </Form.Item>
         
           <Form.Item label="Numéro de téléphone" autocomplete="off" name="téléphone" required className={styles.FormLabel}>
                <span style={{display:'flex', flexDirection:'column'}}>
                    <span><input className={styles.InputRegistration}  placeholder="entrer votre numéro de téléphone" ref={register({required:"Votre numéro de téléphone est recquis"})} name="phoneNumber" /> <img src={profilIcon} alt="" /></span>
                    {errors.phoneNumber && <small className={styles.Error}>{errors.phoneNumber.message}</small>}
                </span>
           </Form.Item>

           <Form.Item label="Sexe"  name="sexe" required className={styles.FormLabel}>
                <Radio.Group onChange={onChangeRadio} value={radioValue} name="gender" color="warning">
                    <Radio  value={"Man"} >Homme</Radio>
                    <Radio value={"Woman"}>Femme</Radio>
                </Radio.Group>
                <img src={genderIcon} style={{float:'right',marginRight:'93px'}} alt="" />
            </Form.Item>

            <Form.Item label="Date de naissance" name="date" required className={styles.FormLabel}>
                <span style={{display:'flex', flexDirection:'column'}}>
                   <span><input className={styles.InputRegistration} type="date" name="birthday" ref={register({required:"Date de naissance obligatoire !"})}/><img src={profilIcon} alt="" /></span>
                    {errors.birthday && <small className={styles.Error}>{errors.birthday.message}</small>}
                </span>
           </Form.Item>

           <Form.Item label="Profession" name="profession" required className={styles.FormLabel}>
                <span style={{display:'flex', flexDirection:'column'}}>
                    <span>      
                        <select  className={styles.InputRegistration} name="profession" placeholder="Chercheur d'emploie" ref={register({required:"Veuillez renseigner votre profession "})} >
                        <option value="EMPLOYEE">Chercheur d'emploie</option>
                        <option value="STUDENT">Etudiant</option>
                        <option value="RECONVERSION">Reconversion</option>
                        </select>
                        <img src={profilIcon} alt="" />
                    </span>
                    {errors.profession && <small className={styles.Error}>{errors.profession.message}</small>}
                </span>
            </Form.Item>

            <Form.Item name="origin" label="Comment avez vous connu Jobaas ?" required className={styles.FormLabel}>
                <span style={{display:'flex', flexDirection:'column'}}>
                    <span>
                        <select  className={styles.InputRegistration} placeholder="Whatsapp" name='origin' ref={register({required:"Veuillez renseigner l'origine "})} >
                                                    <option value="Friends">Amis</option>
                                                    <option value="Facebook">Facebook</option>
                                                    <option value="Instagram">Instagram</option>
                                                    <option value="School_or_training_center">Etablissement/Centre de formation</option>
                                                    <option value="Whatsapp">Whatsapp</option>
                                                    <option value="LinkedIn">LinkedIn</option>
                                                    <option value="Youtube">Youtube</option>
                                                    <option value="Video">Vidéo de réseaux sociaux</option>
                                                    <option value="Taxi">Taxi</option>
                                                    <option value="Influencer">Influenceur</option>
                                                    <option value="Sms">Sms</option>
                                                    <option value="Other">Autre</option>
                        </select> 
                        <img src={profilIcon} alt="" />
                    </span> 
                    {errors.origin && <small className={styles.Error}>{errors.origin.message}</small>}            
                </span>
            </Form.Item>

            <Form.Item label="Mot de passe"  name="motPass" required className={styles.FormLabel}>
                <span style={{display:'flex', flexDirection:'column'}}>
                    <span><input type="password"   className={styles.InputRegistration}  placeholder="entrer votre mot de passe"  ref={register({required:"Votre mot de passe est recquis", pattern: { value:  /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/, message:'Mot de passe invalide. Il doit comporter 8 caractères au minimum. Parmi lesquels au moins, un chiffre, une majuscule un miniscule'}})}  name="password" /> <img src={lockIcon} alt="" /></span>
                    {errors.password && <small className={styles.Error}>{errors.password.message}</small>}
                </span>
            </Form.Item>

            <Form.Item label="Confirmer le Mot de passe" name="CmotPass" required className={styles.FormLabel}>
            <span style={{display:'flex', flexDirection:'column'}}>
                <span> <input disabled={errors.password} type="password"  className={styles.InputRegistration} placeholder="confirmer votre mot de passe" ref={register({required:"S'il vous plait confirmer votre mot de passe", validate: value =>value === password.current || "Les deux mots de passe ne correspondent pas!"})} name="confirmPassword"/>  <img src={lockIcon} alt="" /></span>
                {errors.confirmPassword && <small className={styles.Error}>{errors.confirmPassword.message}</small>}
            </span>
            </Form.Item>

            <Form.Item>
                {!isLoading ?
                                        (
                                            <>
                                                <button disabled={!isValid}  className={styles.Button}>S'inscrire</button>
                                            </>
                                        ) :
                                        (
                                            <>
                                                <Tag icon={<SyncOutlined spin/>}
                                                     style={{color: "white", fontSize: "large"}}
                                                     className={styles.buttonLogin}>
                                                    Validation en cours
                                                </Tag>
                                            </>
                                        )
                                    }
            </Form.Item>

            </Form>
            ):
            (
                 <>
                    <div style={{padding:'25px'}}>
                        <AlertMessage
                            message="Votre compte a été crée avec succès"
                            description={message}
                            isSucceed={true}
                        />
                    </div>
                </>
            )
            
            }
            <span style={{color:'#D04747', fontSize:'1.6em',}}>déjà inscrit? <strong style={{fontWeight:'bold', color:'black'}}>Connectez-vous !</strong></span>
        </div>
    )
}

export default RegistrationForm;