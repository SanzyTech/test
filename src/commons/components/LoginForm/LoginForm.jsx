import React, {useEffect, useState} from 'react';
import * as styles from './LoginForm.module.css';
import {connectedSelector, currentUserSelector, errorLoginSelector, loginAction} from "../../../redux/AuthReducer";
import { Form ,Tag } from 'antd';
import lockIcon from '../../../assets/icons/lockIcon.png';
import emailIcon from '../../../assets/icons/emailIcon.png'
import {useForm} from 'react-hook-form';
import {useMutation} from 'react-query';
import {useDispatch, useSelector} from 'react-redux';
import Alert from 'antd/lib/alert';
import {Link, Redirect} from 'react-router-dom';
import {SyncOutlined} from "@ant-design/icons";
import {login} from "../../../services/auth"
import * as moment from "moment";
import {useLastLocation} from 'react-router-last-location';
import {routesToExclude} from "../../../utils/enums/others";



const LoginForm = (props) => {
    document.title = "Jobaas CV | Se connecter";
    const [messageToDisplay, setMessageToDisplay] = useState('');
    const currentUserLocal = useSelector(currentUserSelector);
    const connected = useSelector(connectedSelector);
    let errorLoginState = useSelector(errorLoginSelector);
    const lastLocation = useLastLocation();
    const [errorState, setErrorState] = useState(0);
    const {register, handleSubmit, formState,errors} = useForm({mode: "onChange"});
    const {isValid} = formState;
    const dispatch = useDispatch();
    const [errorRegister, setErrorRegister] = useState(false);
    // const register = new URLSearchParams(props.location.search).get("register");


    useEffect(() => {
        window.scrollTo(0, 0);
        moment.locale('fr');
        if (register === '2') {
            setMessageToDisplay("Votre compte a été validé avec succès, connectez vous s'il vous plait");
        } else if (register === '1') {
            setMessageToDisplay("Ce compte a déjà été vérifé. Connectez vous s'il vous plait  !");
        } else if (register === '3') {
            setMessageToDisplay("Votre lien d'activation est incorrecte ou a expiré !");
            setErrorRegister(true);
        } else if (register === '4') {
            setMessageToDisplay("Ce compte n'existe pas. Veuillez d'abord vous inscrire !");
            setErrorRegister(true);
        } else if (register === '5') {
            setMessageToDisplay("Une erreur s'est produite, veuillez réessayer");
            setErrorRegister(true);
        }
    }, [register]);

    const [mutate, {isLoading, isError}] = useMutation(
        login,
        {
            onSuccess: (data) => {
                dispatch(loginAction(data.accessToken));
                setErrorState(0);
            },
            onError: (error) => {
                setMessageToDisplay(error.message);
            },
        }
    );

    const onSubmit = async (data) => {
        const dataLogin = {
            login: data.email,
            password: data.password,
        };
        await mutate(dataLogin);
    };

    if (connected) {
        let previousPath =  lastLocation ? lastLocation.pathname : null;
         if (currentUserLocal && currentUserLocal.role.includes('employee')) {
            return previousPath !== null && !routesToExclude.includes(previousPath) ?
                <Redirect to="/app" /> :
                <Redirect to="/app"/>
        } else {
            return previousPath !== null  && !previousPath.startsWith('/fr/ChangePassword') &&
            !routesToExclude.includes(previousPath) ?
                <Redirect to="/rh-home"/> :
                <Redirect to="/rh-home"/>
        }
    }


    if (errorLoginState && (messageToDisplay === '' || errorState === 0)) {
        setMessageToDisplay("");
        setErrorState(1);
    }

    return (
        <div>
            <div className={styles.FormSection}>
                <p className={styles.TitleForm}> CONNEXION </p>

                {((isError || errorLoginState || errorRegister) && (messageToDisplay !== '')) ?
                        (
                            <>
                                <Alert message={messageToDisplay} type="error" showIcon/>
                                <br/>
                            </>
                        ) :
                        null}
                    {
                        (messageToDisplay !== '' && (register === '1' || register === '2') && (!isError || !errorLoginState)) ?
                            (
                                <>
                                    <Alert message={messageToDisplay} type="success" showIcon/>
                                    <br/>
                                </>) : null
                    }


                <Form layout="vertical" onSubmitCapture={handleSubmit(onSubmit)} >
                <Form.Item label="Adresse mail"  rules={[{ required: true}]} required className={styles.FormLabel}>
                <span style={{display:'flex', flexDirection:'column'}}>
                    <span><input className={styles.InputLogin} placeholder="entrer votre adresse email" ref={register({required:"L'adresse email est obligatoire ! "})} name="email" /> <img src={emailIcon}  /></span>
                    {errors.email && <small className={styles.Error}>{errors.email.message}</small>}
                </span>
            </Form.Item>

            <Form.Item label="Mot de passe"  rules={[{ required: true}]} required className={styles.FormLabel}>
                <span style={{display:'flex', flexDirection:'column'}}>
                    <span><input type="password"  className={styles.InputLogin}  placeholder="entrer votre mot de passe"  ref={register({required:"Mot de passe obligatoire ! "})}  name="password" /> <img src={lockIcon} /></span>
                    {errors.password && <small className={styles.Error}>{errors.password.message}</small>}
                </span>
            </Form.Item>

                    <div style={{float:'right', fontWeight:'bold',fontSize:'28px', color:'#F14D4D', marginRight:'60px'}}>Mot de passe oublié?</div>
                    <br/>
                    <br/>
                    <br/>
                    {!isLoading ?
                                    (
                                        <>
                                            <button className={styles.Button} disabled={!isValid} >Connexion</button>
                                        </>
                                    ) :
                                    (
                                        <>
                                            <Tag icon={<SyncOutlined spin/>} style={{color: "white", fontSize: "large", marginRight:'auto', marginLeft:'auto' }}
                                                 className={styles.buttonLogin}>
                                                Connexion en cours
                                            </Tag>
                                        </>
                                    )
                                }

                </Form>
                <span style={{color:'#D04747', fontSize:'1.6em', lineHeight:'38.4px'}}>Pas de compte? <strong style={{fontWeight:'bold', color:'black'}}>Connectez-vous !</strong></span>
            </div>
        </div>
    )
}
 export default LoginForm;