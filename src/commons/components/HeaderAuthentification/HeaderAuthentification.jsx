
import * as styles  from "./HeaderAuthentification.module.css";
import logo3 from '../../../assets/logos/logo3.png';
import {useLocation} from 'react-router-dom'
import RegistrationForm from '../RegistrationForm';
import LoginForm from '../LoginForm';
import { Link } from 'react-router-dom';
import { Carousel } from 'antd';
import { offerlist } from "../../../pages/CustomerModule/OfferListPage/offerlist/offerlist";
import './HeaderAuthentification.module.css'


const HeaderAuthentification = (props) => {

    const currentRoute = useLocation();

    const contentStyle = {
        height: '526px',
        width:'565px',
        color: '#fff',
        background: '#fff',
      };
      

    return (
        
       <div className={styles.SpaceContent }>
           <div className={styles.LeftSection}>
                <nav className={styles.NavBar}>
                    <img alt="" src={logo3} className={styles.Logo} />
                    {currentRoute.pathname === '/register' ?
                                    (
                                        <>
                                            <Link to="/login"><button className={styles.ButtonNav}> Se connecter</button></Link>
                                        </>
                                    ) :
                                    (
                                        <>
                                            <Link to="/register"><button className={styles.ButtonNav}> S'inscrire</button></Link>
                                        </>
                                    )
                                }
                </nav>
                <section className={styles.FormSection}>  
                {currentRoute.pathname === '/register' ?
                                    (
                                        <>
                                            <RegistrationForm/>
                                        </>
                                    ) :
                                    (
                                        <>
                                            <LoginForm/>
                                        </>
                                    )
                                }
                </section>
           </div>
           <div style={{zIndex:'10000000888222'}}>
           {currentRoute.pathname === '/login' ?
                                    (
                                        <div>
                                        <div className={styles.BannerSectionLogin}></div>
                                        </div>
                                    ) :
                                    (
                                        <div style={{display:'flex', flexDirection:'column', width:'40%', position:'fixed'}}>
                                        <div className={styles.BannerSectionRegistration}>
                                        </div>
                                        <div className={styles.OfferSection}>
                                            <h2 className={styles.TitlePlan}>NOS PLANS TARIFAIRES</h2>
                                            <Carousel style={contentStyle} autoplay speed={3200} effect="fade">
                                            {offerlist.map(({id,name,description}) =>
                                                <div key={id}>
                                                    <h4 className={styles.TitleOffer}> {name}</h4>
                                                    <h5 className={styles.Description}>{description}</h5>
                                                 </div>   
                                            )}        
                                            </Carousel>
                                        </div>
                                    </div>
                                    )
                                }
           </div>

       </div>
    )
}

export default HeaderAuthentification;