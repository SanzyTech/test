import React, { useState, useEffect } from 'react';
import * as styles from "./Header.module.css";
import logo3 from '../../../assets/logos/logo3.png';
import {useLocation} from 'react-router-dom';
import HeaderAuthentification from '../HeaderAuthentification';
import { UserOutlined } from '@ant-design/icons';
import { Avatar } from 'antd';
import { DownOutlined } from '@ant-design/icons';
import { Dropdown, Menu, Space } from 'antd';
import { Link } from 'react-router-dom';
import { Button } from 'antd';
import { useSelector } from 'react-redux';
import { connectedSelector, logoutAction } from '../../../redux/AuthReducer';
import { useDispatch } from 'react-redux';

const Header = () => {

  const dispatch = useDispatch();
  const connected = useSelector(connectedSelector);
  const  currentRoute= useLocation();


  const logout = () => {

    dispatch(logoutAction());
  }

    const profilMenuDropdown = (
      <Menu>  
         <Menu.Item key="1">
          <li>Paramètres</li>
        </Menu.Item>
        <Menu.Item key="2">
          <li onClick={logout}>
          <Link to={`/logout`}>
              Se deconnecter
          </Link>
          </li>
        </Menu.Item>
        <Menu.Item key="3">
          <li >
          <Link to={`/offers`}>
              Upgrade souscription
          </Link>
          </li>
        </Menu.Item>
      </Menu>  
    )


    return (
        <>
           

            {(currentRoute.pathname === '/register') ||(currentRoute.pathname ==='/login')  ?
                                    (
                                        <>
                                            <HeaderAuthentification/>
                                        </>
                                    ) :
                                    (
                                        <>
                                            <div className={styles.header}>
                                                 <img alt="" src={logo3} className={styles.Logo} />
                                                 {connected&&(<div className={styles.Avatar}>
                                                              <Link to="/mes-rendez-vous"><Button className={styles.ButtonNav}> Rendez-vous</Button></Link>
                                                              <Link to="/mes-documents"><Button className={styles.ButtonNav}> Mes Documents</Button></Link>
                                                              <Link to="/mes-evaluations"><Button className={styles.ButtonNav}> Evaluations</Button></Link>
                                                              <span>
                                                              <Avatar size={60} icon={<UserOutlined />} />
                                                              <Dropdown overlay={profilMenuDropdown}>
                                                                <a onClick={e => e.preventDefault()}>
                                                                  <Space>
                                                                    <DownOutlined />
                                                                  </Space>
                                                                </a>
                                                              </Dropdown>

                                                              </span>
                                                            </div>)

                                                 }
                                                 
                                            </div>
                                        </>
                                    )
                                }
        </>
    )
};

export default Header;