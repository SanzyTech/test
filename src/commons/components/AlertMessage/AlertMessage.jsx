import {Alert} from "antd";
import React from "react";

const AlertMessage = (props) => {

    const SuccessMessage = () =>{
            return (
                <>
                    <br/>
                    <Alert
                        message={props.message}
                        description={props.description}
                        type="success"
                        showIcon
                    /><br/>
                </>
            )
        };

    const ErrorMessage = () => {
        return (
            <>
                <br/>
                <Alert
                    message={props.message}
                    description={props.description}
                    type="error"
                    showIcon
                /><br/>
            </>
        )
    };

    return (
        <>
            {
                props.isSucceed === true ?
                    SuccessMessage() :
                        props.isError === true ?
                            ErrorMessage() : null
            }
        </>
    )
};

export default AlertMessage
