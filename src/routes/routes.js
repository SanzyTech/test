import React, {Suspense, lazy} from 'react';
import {BrowserRouter as Router, Redirect, Route, Switch} from 'react-router-dom';
import {AVAILABLE_LANG} from "../utils/enums/others";
import {useSelector} from "react-redux";
import {connectedSelector} from "../redux/AuthReducer";
import Loader from "../commons/components/Loader";
import {LastLocationProvider} from 'react-router-last-location';

import Header from "../commons/components/Header";
// import Footer from "../commons/components/Footer/Footer";
import HomePage from '../pages/CustomerModule/HomePage/view/HomePage';
import FrontOffice from '../pages/CustomerModule/FrontOffice/View/FrontOffice';
import RegistrationForm from '../commons/components/RegistrationForm';
import DocumentPage from '../pages/CustomerModule/DocumentPage/view/DocumentPage';
import EvaluationPage from '../pages/CustomerModule/EvaluationPage/view/EvaluationPage';
import RhEvaluationPage from '../pages/RhModule/RhEvaluationPage/view/RhEvaluationPage';
import RhHomePage from '../pages/RhModule/RhHomePage/view/RhHomePage';
import MeetingPage from '../pages/CommonModule/Meeting/view/MeetingPage';
import OfferListPage from '../pages/CustomerModule/OfferListPage/View/OfferListPage';


const Home = lazy(()=> import('../pages/CommonModule/Home'))
// const LoginForm = lazy(()=> import('../commons/components/LoginForm'))
const Login = lazy(()=> import('../pages/CommonModule/Login'))
const NotFound = lazy(()=> import('../pages/CommonModule/NotFound'))

// Url mapping
export const Urls = {
    // home: {
    //     path: "/",
    //     component: Home,
    //     protected: false
    // },

    // login: {
    //     component: LoginForm,
    //     protected: false
    // },
    register: {
        path:"/register",
        component: RegistrationForm,
        protected:false
    },
    homepage: {
        path: "/home",
        component: HomePage,
        protected: false,
    },
    rhhomepage: {
        path: "/rh-home",
        component: RhHomePage,
        protected: false,
    },
    documents: {
        path: "/documents",
        component: DocumentPage,
        protected: false,
    },
    evaluations: {
        path: "/mes-rendez-vous",
        component: EvaluationPage,
        protected: false,
    },
    meetings: {
        path: "/mes-meetings",
        component: MeetingPage,
        protected: false,
    },
    pffers: {
        path: "/offers",
        component: OfferListPage,
        protected: false,
    },
    logout: {
        path: "/logout",
        component: <redirect to="/login?lang=fr"/>,
        protected: true,
        scopes: "all"
    }
};

const ProtectedRoute = (props) => {
    const connected = useSelector(connectedSelector);
    localStorage.setItem('previousPath', props.path);
    return connected ?
        <Route exact path={props.path} component={props.component}/>
        : <Redirect to="/login?lang=fr"/>
};

const Routes = () => {
    const connected = useSelector(connectedSelector);
    // Keys of Urls object that are used for creating Routes
    let urlsKeys = [];
    for(let key in Urls){
        urlsKeys.push(key)
    }
    return (
        <>
            <Router>
            <Header/>
                <Suspense fallback={Loader}>
                    <LastLocationProvider>
                        <Switch>
                            {urlsKeys.map((entry, entryIndex) => (
                                AVAILABLE_LANG.map((lang, langIndex) => (
                                    Urls[entry].protected ?
                                        <ProtectedRoute
                                            key={`${entryIndex} - ${langIndex}`}
                                            path={`/${lang}${Urls[entry].path}`}
                                            component={Urls[entry].component}
                                        /> : <Route exact
                                                    key={`${entryIndex} - ${langIndex}`}
                                                    path={`/${lang}${Urls[entry].path}`}
                                                    component={Urls[entry].component}
                                        />
                                ))
                            ))};
                                        <Route
                                                exact
                                                path="/"
                                                render={() => {
                                                    return (
                                                    connected?
                                                    <Redirect to="/app" /> :
                                                    <Redirect to="/register" /> 
                                                    )
                                                }}
                                            />
                            {/* <Route component={NotFound} /> */}
                            <Route exact path="/app" component ={FrontOffice}/>
                            <Route exact path="/mes-documents" component ={DocumentPage}/>
                            <Route exact path="/mes-evaluations" component ={EvaluationPage}/>
                            <Route exact path="/rh-home" component ={RhHomePage}/>
                            <Route exact path="/rh-evaluation" component ={RhEvaluationPage}/>
                            <Route exact path="/mes-rendez-vous" component ={MeetingPage}/>
                            <Route exact path="/offers" component ={OfferListPage}/>
                            
                        </Switch>
                    </LastLocationProvider>
                </Suspense>
                {/* <Footer /> */}
            </Router>
        </>
    )
};

export default Routes;
